adjusttext==0.7.3
alabaster==0.7.12; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
altair-data-server==0.4.1; python_version >= "3.6"
altair-saver==0.5.0; python_version >= "3.6"
altair-viewer==0.4.0; python_version >= "3.6"
altair==4.2.0; python_version >= "3.7"
appnope==0.1.2; platform_system == "Darwin" and python_version >= "3.8" and sys_platform == "darwin"
argon2-cffi-bindings==21.2.0; python_version >= "3.6"
argon2-cffi==21.3.0; python_version >= "3.6"
asgiref==3.5.0; python_version >= "3.7" and python_full_version >= "3.6.2"
astroid==2.9.3; python_full_version >= "3.6.2"
asttokens==2.0.5; python_version >= "3.8"
attrs==21.4.0; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.7"
babel==2.9.1; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
backcall==0.2.0; python_version >= "3.8"
beautifulsoup4==4.10.0; python_full_version > "3.0.0" and python_version >= "3.6"
black==21.12b0; python_full_version >= "3.6.2"
bleach==4.1.0; python_version >= "3.7"
bokeh==2.4.2; python_version >= "3.7"
branca==0.4.2; python_version >= "3.5"
brotli==1.0.9; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
cachetools==4.2.4; python_version >= "3.5" and python_version < "4.0"
cairocffi==1.3.0; python_version >= "3.7"
cairosvg==2.5.2; python_version >= "3.6"
cartopy==0.20.2; python_version >= "3.7"
certifi==2021.10.8; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.7"
cffi==1.15.0; implementation_name == "pypy" and python_version >= "3.7" and python_full_version >= "3.6.1"
cfgv==3.3.1; python_full_version >= "3.6.1"
charset-normalizer==2.0.12; python_full_version >= "3.6.0" and python_version >= "3.5"
click-plugins==1.1.1; python_version >= "3.6"
click==8.0.3; python_version >= "3.8" and python_full_version >= "3.6.2" and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6") and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version < "4" and python_version >= "3.6")
cligj==0.7.2; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version < "4" and python_version >= "3.6"
colorama==0.4.4; python_full_version >= "3.6.2" and platform_system == "Windows" and sys_platform == "win32" and python_version >= "3.6" and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6") and (python_version >= "3.5" and python_full_version < "3.0.0" and sys_platform == "win32" and (python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5") or sys_platform == "win32" and python_version >= "3.5" and (python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5") and python_full_version >= "3.5.0") and (python_version >= "3.8" and python_full_version < "3.0.0" and sys_platform == "win32" or sys_platform == "win32" and python_version >= "3.8" and python_full_version >= "3.5.0")
coreapi==2.3.3
coreschema==0.0.4
coverage==5.5; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.5.0" and python_version < "4")
crispy-bootstrap5==0.6; python_version >= "3.6"
crispy-forms-materialize==0.2
cssselect2==0.4.1; python_version >= "3.6"
cycler==0.11.0; python_version >= "3.7"
dash-core-components==1.16.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
dash-html-components==1.1.3; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
dash-renderer==1.9.1; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
dash-table==4.11.3; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
dash==1.20.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
debugpy==1.5.1; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.7"
decorator==5.1.1; python_version >= "3.8"
defusedxml==0.7.1; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.7"
deprecated==1.2.13; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.6"
descartes==1.1.0; python_version >= "3.6"
diff-match-patch==20200713; python_version >= "3.6"
distlib==0.3.4; python_full_version >= "3.6.1"
django-admin-numeric-filter==0.1.6
django-admin-tools==0.9.2
django-admin==2.0.1
django-appconf==1.0.5; python_version >= "3.6"
django-auth-kerberos==1.2.5
django-auth-ldap==3.0.0; python_version >= "3.6"
django-bootstrap3==15.0.0; python_version >= "3.6"
django-bootstrap4==3.0.1; python_version >= "3.6"
django-cachalot==2.5.0
django-cleanup==5.2.0
django-codemirror2==0.2
django-common-helpers==0.9.2
django-compressor==2.4.1
django-crispy-forms==1.14.0; python_version >= "3.7"
django-cron==0.5.1
django-cte==1.1.4
django-dbbackup==3.3.0
django-debug-toolbar==3.2.4; python_version >= "3.6"
django-excel-base==1.0.4
django-excel-response2==3.0.2
django-extensions==3.1.5; python_version >= "3.6"
django-filter==2.4.0; python_version >= "3.5"
django-geojson==3.2.0; python_version >= "3.5"
django-guardian==2.4.0; python_version >= "3.5"
django-import-export==2.7.1; python_version >= "3.6"
django-js-asset==2.0.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.6"
django-leaflet==0.28.2
django-markdownx==3.0.1
django-menu-generator==1.1.0
django-modeltranslation==0.17.5; python_full_version >= "3.6.2"
django-mptt==0.9.1; python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.4.0"
django-plotly-dash==1.6.6; python_version >= "3.6"
django-redis==5.2.0; python_version >= "3.6"
django-rest-swagger==2.2.0
django-reversion==4.0.2; python_version >= "3.6"
django-river==3.3.0
django-select2==7.10.0
django-sendmail-backend==0.1.2
django-simple-history==3.0.0; python_version >= "3.6"
django-six==1.0.4
django-split-settings==1.1.0; python_version >= "3.6" and python_version < "4.0"
django-sql-explorer==2.4; python_version >= "3.6"
django-tables2==2.4.1
django-tracking2==0.5.0
django-treenode==0.17.1
django==3.1.8; python_version >= "3.6"
djangorestframework==3.13.1; python_version >= "3.6"
docutils==0.16; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.5"
dpd-components==0.1.0; python_version >= "3.6"
entrypoints==0.4; python_full_version >= "3.6.1" and python_version >= "3.7"
et-xmlfile==1.1.0; python_version >= "3.7"
executing==0.8.2; python_version >= "3.8"
feedparser==6.0.8; python_version >= "3.6"
filelock==3.5.0; python_version >= "3.7" and python_full_version >= "3.6.1"
fiona==1.8.21
flask-compress==1.10.1; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
flask==2.0.3; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
folium==0.12.1.post1; python_version >= "3.5"
fonttools==4.29.1; python_version >= "3.7"
future==0.18.2; (python_version >= "2.6" and python_full_version < "3.0.0") or (python_full_version >= "3.3.0")
geopandas==0.9.0; python_version >= "3.6"
h2o==3.36.0.3
html5lib==1.1; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.6"
identify==2.4.10; python_version >= "3.7" and python_full_version >= "3.6.1"
idna==3.3; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.5"
imagesize==1.3.0; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
importlib-metadata==4.11.1; python_version < "3.10" and python_version >= "3.7"
ipykernel==6.9.1; python_version >= "3.7"
ipython-genutils==0.2.0; python_version >= "3.7"
ipython==8.0.1; python_version >= "3.8"
ipywidgets==7.6.5
isort==5.10.1; python_full_version >= "3.6.2" and python_version < "4.0"
itsdangerous==2.0.1; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
itypes==1.2.0
jedi==0.18.1; python_version >= "3.8"
jinja2==3.0.3; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.7"
jsonfield==3.1.0; python_version >= "3.6"
jsonschema==4.4.0; python_version >= "3.7"
jupyter-client==7.1.2; python_full_version >= "3.7.0" and python_version >= "3.7"
jupyter-console==6.4.0; python_version >= "3.6"
jupyter-core==4.9.1; python_full_version >= "3.6.1" and python_version >= "3.7"
jupyter==1.0.0
jupyterlab-pygments==0.1.2; python_version >= "3.7"
jupyterlab-widgets==1.0.2; python_version >= "3.6"
kiwisolver==1.3.2; python_version >= "3.7"
lazy-object-proxy==1.7.1; python_version >= "3.6" and python_full_version >= "3.6.2"
lxml==4.7.1; python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.5.0"
markdown==3.3.6; python_version >= "3.6"
markuppy==1.14; python_version >= "3.7"
markupsafe==2.0.1; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.7"
martor==1.6.7
matplotlib-inline==0.1.3; python_version >= "3.5"
matplotlib==3.5.1; python_version >= "3.7"
mccabe==0.6.1; python_full_version >= "3.6.2"
mistune==0.8.4; python_version >= "3.7"
mizani==0.7.3; python_version >= "3.6"
munch==2.5.0; python_version >= "3.6"
mypy-extensions==0.4.3; python_full_version >= "3.6.2" and python_version >= "3.8"
nbclient==0.5.11; python_full_version >= "3.7.0" and python_version >= "3.7"
nbconvert==6.4.2; python_version >= "3.7"
nbformat==5.1.3; python_full_version >= "3.7.0" and python_version >= "3.7"
nest-asyncio==1.5.4; python_full_version >= "3.7.0" and python_version >= "3.7"
nodeenv==1.6.0; python_full_version >= "3.6.1"
notebook==6.4.8; python_version >= "3.6"
numpy==1.22.2; python_version >= "3.8"
odfpy==1.4.1; python_version >= "3.7"
openapi-codec==1.3.2
opencv-contrib-python-headless==4.5.5.62; python_version >= "3.6"
opencv-python-headless==4.5.5.62; python_version >= "3.6"
openpyxl==3.0.9; python_version >= "3.6"
packaging==21.3; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.7"
palettable==3.3.0; python_version >= "3.6"
pandas==1.4.1; python_version >= "3.8"
pandocfilters==1.5.0; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.7"
parso==0.8.3; python_version >= "3.8"
pathspec==0.9.0; python_full_version >= "3.6.2" and python_version >= "3.8"
patsy==0.5.2; python_version >= "3.7"
pexpect==4.8.0; sys_platform != "win32" and python_version >= "3.8"
pickleshare==0.7.5; python_version >= "3.8"
pillow==9.0.1; python_version >= "3.7"
platformdirs==2.5.0; python_version >= "3.8" and python_full_version >= "3.6.2"
plotly==5.6.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
plotnine==0.8.0; python_version >= "3.6"
portpicker==1.5.0; python_version >= "3.6"
pre-commit==2.17.0; python_full_version >= "3.6.1"
prometheus-client==0.13.1; python_version >= "3.6"
prompt-toolkit==3.0.28; python_full_version >= "3.6.2" and python_version >= "3.8"
psutil==5.9.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.6"
psycopg2==2.9.3; python_version >= "3.6"
ptyprocess==0.7.0; os_name != "nt" and python_version >= "3.8" and sys_platform != "win32"
pure-eval==0.2.2; python_version >= "3.8"
py==1.11.0; implementation_name == "pypy" and python_version >= "3.7" and python_full_version >= "3.6.1"
pyasn1-modules==0.2.8; python_version >= "3.6"
pyasn1==0.4.8; python_version >= "3.6"
pycparser==2.21; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.7"
pygments==2.11.2; python_version >= "3.8" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.8"
pygraphviz==1.9; python_version >= "3.8"
pykerberos==1.2.1
pylint==2.12.2; python_full_version >= "3.6.2"
pypandoc==1.7.2; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.6.0")
pyparsing==3.0.7; python_version >= "3.7"
pyphen==0.12.0; python_version >= "3.7"
pyproj==3.2.0; python_version >= "3.7"
pyrsistent==0.18.1; python_version >= "3.7"
pyshp==2.2.0; python_version >= "3.7"
python-dateutil==2.8.2; python_full_version >= "3.6.1" and python_version >= "3.8"
python-docx==0.8.11
python-ldap==3.4.0; python_version >= "3.6"
python-magic==0.4.25; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.5.0")
python-pptx==0.6.21
pytz-deprecation-shim==0.1.0.post0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.6"
pytz==2021.3; python_version >= "3.8" and python_full_version >= "3.6.2" and (python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5")
pywin32==303; sys_platform == "win32" and platform_python_implementation != "PyPy" and python_version >= "3.7" and python_full_version >= "3.6.1"
pywinpty==2.0.2; os_name == "nt" and python_version >= "3.7"
pyyaml==6.0; python_version >= "3.7" and python_full_version >= "3.6.1"
pyzmq==22.3.0; python_full_version >= "3.6.1" and python_version >= "3.7"
qtconsole==5.2.2; python_version >= "3.6"
qtpy==2.0.1; python_version >= "3.6"
rcssmin==1.0.6
redis==4.1.3; python_version >= "3.6"
requests==2.27.1; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.5"
retrying==1.3.3
rjsmin==1.1.0
rpy2==3.4.5
scipy==1.8.0; python_version >= "3.8" and python_version < "3.11"
screen==1.0.1
selenium==3.141.0
send2trash==1.8.0; python_version >= "3.6"
sentry-sdk==1.5.10
setuptools-scm==6.4.2; python_version >= "3.7"
sgmllib3k==1.0.0; python_version >= "3.6"
shapely==1.8.0; python_version >= "3.7"
simplejson==3.17.6; python_version >= "2.5" and python_full_version < "3.0.0" or python_full_version >= "3.3.0"
simplekml==1.3.6
six==1.16.0; python_full_version >= "3.6.2" and python_version >= "3.6" and (python_version >= "2.7" and python_full_version < "3.0.0" or python_full_version >= "3.3.0") and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6") and (python_version >= "3.8" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.8") and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.6") and (python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.7")
snowballstemmer==2.2.0; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
soupsieve==2.3.1; python_full_version > "3.0.0" and python_version >= "3.6"
sphinx-rtd-theme==1.0.0; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.4.0")
sphinx==3.5.4; python_version >= "3.5"
sphinxcontrib-applehelp==1.0.2; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
sphinxcontrib-devhelp==1.0.2; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
sphinxcontrib-htmlhelp==2.0.0; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.6"
sphinxcontrib-jsmath==1.0.1; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
sphinxcontrib-plantuml==0.21
sphinxcontrib-qthelp==1.0.3; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
sphinxcontrib-serializinghtml==1.1.5; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
sqlparse==0.4.2; python_version >= "3.6" and python_full_version >= "3.6.2"
stack-data==0.2.0; python_version >= "3.8"
statsmodels==0.13.2; python_version >= "3.7"
tablib==3.2.1; python_version >= "3.7"
tabulate==0.8.9
tblib==1.7.0; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.5.0")
tenacity==8.0.1; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
terminado==0.13.1; python_version >= "3.7"
testpath==0.5.0; python_version >= "3.7"
tinycss2==1.1.1; python_version >= "3.6"
toml==0.10.2; python_full_version >= "3.6.2"
tomli==1.2.3; python_version >= "3.8" and python_full_version >= "3.6.2"
toolz==0.11.2; python_version >= "3.7"
tornado==6.1; python_full_version >= "3.6.1" and python_version >= "3.7"
tqdm==4.62.3; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.4.0")
traitlets==5.1.1; python_full_version >= "3.7.0" and python_version >= "3.8"
typing-extensions==4.1.1
tzdata==2021.5; platform_system == "Windows" and python_version >= "3.6" and (python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version >= "3.6")
tzlocal==4.1; python_version >= "3.6"
uritemplate==4.1.1; python_version >= "3.6"
urllib3==1.26.8; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.6.0" and python_version < "4" and python_version >= "3.6"
vega-datasets==0.9.0; python_version >= "3.5"
virtualenv==20.13.1; python_full_version >= "3.6.1"
wcwidth==0.2.5; python_full_version >= "3.6.2" and python_version >= "3.8"
weasyprint==52.5; python_version >= "3.6"
webencodings==0.5.1; python_version >= "3.7" and python_full_version < "3.0.0" or python_full_version >= "3.5.0" and python_version >= "3.7"
werkzeug==2.0.3; python_version >= "3.6" and python_full_version < "3.0.0" or python_full_version >= "3.3.0" and python_version >= "3.6"
widgetsnbextension==3.5.2
wrapt==1.13.3; python_full_version >= "3.6.2" and python_version >= "3.6"
xlrd==2.0.1; python_version >= "3.7" and python_full_version < "3.0.0" or python_version >= "3.7" and python_full_version >= "3.6.0"
xlsxwriter==3.0.2; python_version >= "3.4"
xlwt==1.3.0; python_version >= "3.7"
zipp==3.7.0; python_version < "3.10" and python_version >= "3.7"
