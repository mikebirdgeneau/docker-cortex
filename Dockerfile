FROM debian:stable-slim
ENV TZ America/Edmonton
ENV DEBIAN_FRONTEND noninteractive

# Set-up Apt
COPY apt_preferences /etc/apt/preferences
RUN echo "deb http://deb.debian.org/debian testing main" >> /etc/apt/sources.list

# Install required packages for application using apt
RUN apt-get update && apt-get upgrade -y && apt-get install -yq apt-utils && apt-get install -yq locales locales-all csh bash

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Install base packages
RUN apt-get install -yq g++ gcc git cron procps rsyslog \
 build-essential checkinstall postgresql-client postgresql-client-common \
 libreadline-dev libncursesw5-dev libssl-dev \
 libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev \
 postgis libgdal-dev binutils libproj-dev gdal-bin proj-bin libmagic1 \
 unixodbc-dev libffi-dev libkrb5-dev postfix sudo bsd-mailx \
 build-essential libssl-dev ca-certificates libpq-dev libpangoft2-1.0-0 \
 libldap2-dev libsasl2-dev slapd ldap-utils tox lcov valgrind \
 vim wget locales sssd-krb5 sssd-krb5-common nis autofs nfs-common \
 gettext task-chinese-s xvfb chromium-driver graphviz libgraphviz-dev \
 pandoc libgdal-dev libpoppler-cpp-dev

# Install libproj from testing
RUN apt-get install -yq -t testing libproj-dev proj-bin

# Install Python & R
RUN apt-get install -yq python3 python3-dev python3-pip python3-venv  \
 uwsgi uwsgi-plugin-python3 \
 python3-mpltoolkits.basemap python-mpltoolkits.basemap-data \
 python3-cartopy python-cartopy-data \
 r-base r-base-dev

# Default JRE
RUN apt-get install -yq default-jre

# Node.js (for Vue.js front-end)
RUN curl -fsSL https://deb.nodesource.com/setup_15.x | bash -
RUN apt-get install -y nodejs npm yarn
RUN npm install -g @vue/cli @vue/cli-service-global

# Python 3.9
WORKDIR /opt
RUN wget -nv https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tgz
RUN tar -xzf Python-3.9.4.tgz
WORKDIR /opt/Python-3.9.4
RUN ./configure --enable-optimizations && make altinstall

# Use python3.9
RUN update-alternatives --install /usr/bin/python python /usr/local/bin/python3.9 3

# Install a few R Packages
RUN mkdir -p /usr/local/lib/R/site-library #&& chmod 777 /usr/local/lib/R/site-library
WORKDIR /site
COPY Rpackages.R /site/Rpackages.R
RUN /usr/bin/Rscript /site/Rpackages.R

WORKDIR /site
COPY requirements.txt /site/requirements.txt
RUN /usr/bin/python -m venv /site/env
RUN . /site/env/bin/activate && python -m pip install --no-cache-dir -U Cython wheel && \
    python -m pip install --upgrade pip
RUN . /site/env/bin/activate && \
    python -m pip install --no-cache-dir -U -r /site/requirements.txt && \
    python -m pip install --no-cache-dir "requests[security]" && \
    python -m pip uninstall -y shapely && \
    python -m pip install --no-cache-dir shapely --no-binary shapely

FROM debian:stable-slim as app

# Install R / Python
COPY --from=builder /opt/Python-3.9.4/python /usr/local/bin/python3.9
RUN update-alternatives --install /usr/bin/python python /usr/local/bin/python3.9 3
RUN apt-get update -yq && apt-get install -yq r-base

# Copy python env
COPY --from=builder /site/env /site/env
# Copy R packages
COPY --from=builder /usr/local/lib/R/site-library /usr/local/lib/R/site-library

# Natural Earth Files (static download)
WORKDIR /root
COPY ne_data.tar.gz /root/ne_data.tar.gz
RUN tar -xzf ne_data.tar.gz

# Set-up Xvfb for any headless web requirements
WORKDIR /site
RUN echo "#!/bin/sh" > /site/Xvfb.start
RUN echo "/usr/bin/Xvfb :88 -ac -screen 0 1366x768x16 &" >> /site/Xvfb.start
RUN echo "export DISPLAY=88.0" >> /site/Xvfb.start
RUN chmod +x /site/Xvfb.start

# Add crontab with clean-up job.
COPY cleanup-cron /etc/cron.d/cron.prod.cleanup.txt
RUN chmod 0644 /etc/cron.d/cron.prod.cleanup.txt
RUN cat /etc/cron.d/cron.prod.*.txt | crontab -

# Set hostname to xxxx.localhost
RUN echo -e "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
RUN echo 'export PS1="\\[$(tput setaf 1)\\]\\u@\\h:\\w CORTEX-PROD# \\[$(tput sgr0)\\]"' >> ~/.bashrc
